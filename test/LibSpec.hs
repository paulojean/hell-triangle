module LibSpec where

import Test.Hspec
import Lib

spec :: Spec
spec = context "hell triangle" $ do
  describe "max value from triangle" $ do
    it "from empty list" $ do
      maxFromTriangle [] `shouldBe` 0
    it "from list with single element" $ do
      maxFromTriangle [[1]] `shouldBe` 1
    it "from stard list" $ do
      maxFromTriangle [[6],[3,5],[9,7,1],[4,6,8,4]] `shouldBe` 26
    it "from stard list" $ do
      maxFromTriangle [[2]
                      ,[2,1]
                      ,[4,4,2]
                      ,[2,3,2,1]
                      ,[5,0,1,2,0]
                      ,[6,2,5,4,2,100]] `shouldBe` 106
