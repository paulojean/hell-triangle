module Main where

import Lib
import System.Environment

main :: IO ()
main = do
  args     <- getArgs
  triangle <- return (read (head args) :: [[Int]])
  return (maxFromTriangle triangle) >>= print
