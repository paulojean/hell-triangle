# Hell-Triangles

## Requirements
- [haskellstack](https://docs.haskellstack.org/)

## Build
```sh
stack build
```

## Run tests
```sh
stack test
```

## Execute from command line
```sh
stack exec hell-triangle-exe [[6],[3,5],[9,7,1],[4,6,8,4]]
```

### Why Haskell?
- Its type system makes easy to express the solution
- Its lazy. This helps, eg: [here](https://github.com/paulojean/hell-triangle/blob/master/src/Lib.hs#L18) in each step `sumNode` will process a maximum of two elements from current row
