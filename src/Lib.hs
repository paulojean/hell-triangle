module Lib
    ( maxFromTriangle
    , Node (..)
    ) where

data Node = Node { col :: Int
                 , val :: Int
                 } deriving (Show)

sumNode :: [Int] -> Node -> [Node]
sumNode (x:[])   (Node col val) = [Node col (val + x)]
sumNode (x:y:ys) (Node col val) = [Node col (val + x)
                                  ,Node (col + 1) (val + y)]

sumNodes :: [Node] -> [[Int]] -> [Node]
sumNodes nodes []     = nodes
sumNodes nodes (x:xs) = sumNodes (concatMap sum nodes) xs
  where sum node@(Node col _) = sumNode (drop col x) node

nodeWithMaxVal :: [Node] -> Node
nodeWithMaxVal (x:xs) = foldl maxValue x xs
  where maxValue n1@(Node _ val1) n2@(Node _ val2) =
          if val1 > val2 then n1 else n2

maxFromTriangle :: [[Int]] -> Int
maxFromTriangle []       = 0
maxFromTriangle ([x]:xs) = val $ nodeWithMaxVal $ sumNodes [Node 0 x] xs
